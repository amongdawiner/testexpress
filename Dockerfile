FROM node:lts
WORKDIR /app
COPY package*.json ./
RUN mkdir /.npm && chown -R 1000830000:0 "/.npm"
RUN npm install --strict-ssl=false
COPY . .
EXPOSE 3000
CMD ["node", "server"]
