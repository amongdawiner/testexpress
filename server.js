const express = require('express');
const app = express();
const port = 3000;

// Middleware to parse JSON request body
app.use(express.json());

// Example data
let users = [
  { id: 1, name: 'John Doe' },
  { id: 2, name: 'Jane Smith' }
];

// GET /users - Get all users
app.get('/', (req, res) => {
  res.json(users);
});


// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});